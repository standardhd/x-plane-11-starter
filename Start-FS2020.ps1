<#
X-Planer 11 Starter
a tool to startup applications in order for your flight experience

Author: Jens Stroede

Version 0.5

#>

#Load arrays

$processlist = Get-Content @("$PSScriptRoot/process.txt")
$servicelist = Get-Content @("$PSScriptRoot/services.txt")
$applist = Get-Content @("$PSScriptRoot/app.txt")

# 1st Kill unwanted processes
foreach ($process in $processlist) {
    Stop-Process -name  $process 
} 

# 2nd stop services (adminsitrator rights needed!)

foreach ($service in $servicelist) {
    Stop-Service -Name  $service -Force
}

foreach ($app in $applist) {
    $processdetail = Split-Path "$app" -LeafBase
    Write-Host $processdetail
    if ((get-process -Name "$processdetail" -ea SilentlyContinue) -eq $Null) { 
        Write-Host -ForegroundColor Yellow "$app not running, starting"
        start-process -filepath "$app"
        Start-Sleep -s 6
    }
    else {
            Write-Host "$app already running"
        }  
}