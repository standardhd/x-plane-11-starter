# X-PLane 11 Starter

This is a simple PowerShell script.

You will need at least Powershell 6 if you don´t want to use the EXE-File

Find Powershell 7 [here](https://github.com/PowerShell/PowerShell/releases/tag/v7.0.0)

It will read three files
- process.txt
- service.txt
- app.txt 

How-To install

Extract the files into a direcotry of your choice.

**IMPORTANT**
The files have to be in the same folder

Execute the script ( .ps1 or .exe ) with admin privilges to be able to kill processes and stop services


Edit the Textfile at your choice
Every program , process and service needs to be a line in *.txt document.
The list will be gone through in order of the files stated in the *.txt documents
So X-Plane should be the last in the entry.